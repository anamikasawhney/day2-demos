﻿namespace Day2_Project
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //int[] num = new int[10];
            //Console.WriteLine("Enter Numbers");
            //for(int i = 0; i < 10; i++)
            //{
            //    num[i] = Byte.Parse(Console.ReadLine());
            //}
            // declaration & initialization of array
            int[] num = new int[] { 1, 2,3 ,4,10,90,89};

            Console.WriteLine("Numbers are ");
            for (int i = 0; i < num.Length; i++)
            {
                num[i] += 10;
                Console.WriteLine(num[i]);
            }
            //foreach(type range_variable in collection/array)
            // cw(range_variable)

            foreach (int temp in num)
                //temp += 10;
                Console.WriteLine(temp);


        }
    }
}