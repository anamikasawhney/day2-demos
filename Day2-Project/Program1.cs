﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2_Project
{
    internal class Program1
    {
        static void Main()
        {
            String[] names = new string[] { "deepak", "sagar", "lalit", "deepa", "meena" };
            foreach (string name in names)
            {
                Console.WriteLine(name);
            }
            // reverse array
            Array.Reverse(names);
            foreach (string name in names)
            {
                Console.WriteLine(name);
            }
            Array.Sort(names);
            Console.WriteLine("Names in sorted order");
            foreach (string name in names)
            {
                Console.WriteLine(name);
            }

            string sentence = "this is a session for C#,C++ no 002";
            Console.WriteLine("Length" + sentence.Length);
            // find no of words in sentence
            string[] words= sentence.Split(new char[] {' ',','});
            Console.WriteLine("No of words are " + words.Length);
            // fing no of integers in sentence
            int numCount = 0;
            foreach (char temp in sentence)
            {
                if (char.IsDigit(temp))
                {
                    numCount++;
                }
            }
            Console.WriteLine("No of integers are " + numCount);

            Console.WriteLine(sentence.Contains("s"));
            Console.WriteLine(sentence.IndexOf("i"));
            Console.WriteLine(sentence.Replace("s", "S"));
            Console.WriteLine(sentence);
            Console.WriteLine(sentence.Remove(2));
            Console.WriteLine(sentence.ToUpper());
            Console.WriteLine(sentence.ToLower());

        }
    }
}
