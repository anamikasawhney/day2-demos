﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2_Project
{
    internal class Program2
    {
        static void Main()
        {
            //int[,] matrix = new int[3, 3];
            //Console.WriteLine(matrix.GetLength(0));
            //Console.WriteLine(matrix.GetLength(1));

            //for (int i = 0; i < matrix.GetLength(0); i++)
            //{
            //     for(int j=0;j<matrix.GetLength(1); j++)
            //    {
            //        matrix[i,j] = Byte.Parse(Console.ReadLine());
            //    }
            //}

            int[,] matrix = new int[,]
            {
                {1,2,3 },
                {3,4,7 },
                {10,5,6 }
            };
            Console.WriteLine("Elements of Matrix are ");
            for(int i=0;i<matrix.GetLength(0);i++)
            {
                for(int j = 0;j<matrix.GetLength(1);j++)
                {
                    Console.Write(matrix[i, j] + "\t");
                }
                Console.WriteLine();
            }
        }
    }

}
