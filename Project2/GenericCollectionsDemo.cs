﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
    internal class GenericCollectionsDemo
    {
        static void Main()
        {
            Stack<int> stack = new Stack<int>();
            stack.Push(1); 

            Queue<float> queue = new Queue<float>();    
            queue.Enqueue(10.6f);

            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);

            foreach(int x in list)
            {
                Console.WriteLine(x);
            }

            Dictionary<string, int> scores = new Dictionary<string, int>();
            scores["ajay"] = 90;
            scores["deepak"] = 98;
        }
    }
}
