﻿using System;
using System.Collections;
namespace Project2

{
    internal class Program
    {
        static void Main(string[] args)
        {
           // Collection
           // collection is a namespace
           // which contains some classes , ArrayList, Stack, Queue, HashSet, etc

            ArrayList list = new ArrayList();  // collections are dynamic
                                       // 
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add("ajay");
           
            list.Add(10.8f);

            foreach(var i in list)
            {
                Console.WriteLine(i);
            }

            list.Insert(0, 200);
            list.Insert(2, 300);
            Console.WriteLine("After inserting");
            foreach (int i in list)
            {
                Console.WriteLine(i);
            }

            list.Remove(200);
            list.RemoveAt(1);
            Console.WriteLine("After deleting");
            foreach (int i in list)
            {
                Console.WriteLine(i);
            }


            // LIFO

            Stack stack = new Stack();
            stack.Push(100);
            stack.Push(200);
            stack.Push("aaaaaa");
            stack.Push(300);
            Console.WriteLine("ELements of stacck");
            foreach (int i in stack)
            {
                Console.WriteLine(i);
            }

            stack.Pop();
            Console.WriteLine("ELements of stacck after deletion");
            foreach (int i in stack)
            {
                Console.WriteLine(i);
            }

            //FIFO
            Queue queue = new Queue();  
            queue.Enqueue(100);
            queue.Enqueue(200);
            queue.Enqueue(400);
            Console.WriteLine("ELements of queue");
            foreach (int i in queue)
            {
                Console.WriteLine(i);
            }

            queue.Dequeue();
            Console.WriteLine("ELements of queue after deletion");
            foreach (int i in queue)
            {
                Console.WriteLine(i);
            }

            foreach(int x in list)
            {
                if (x == 200)
                {
                    Console.WriteLine("Found");
                }
            }


            Hashtable ht = new Hashtable();
            // values are stored in <key,value> pair
            ht[100] =90;
            ht[200] = 98;

            foreach(int key in ht.Keys)
            {
                Console.WriteLine($"Score of {key} is {ht[key]}");
            }

            //ht[200]
        }
    }
}